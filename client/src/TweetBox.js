import { Avatar, Button } from "@material-ui/core";
import React, { useState } from "react";
import "./TweetBox.css";
import pic from "./prof-pic.jpg";
import db from "./firebase";

function TweetBox() {
  const [tweetMessage, setTweetMessage] = useState("");
  const [tweetImage, setTweetImage] = useState("");

  const sendTweet = (e) => {
    e.preventDefault();
    // stops refresh
    db.collection("posts").add({
      displayName: "Jalyn Boyd",
      username: "Fluco",
      verified: true,
      image: tweetImage,
      text: tweetMessage,
      avatar:
        "https://pbs.twimg.com/profile_images/1217259689072824320/HA0yuBZL.jpg",
    });
    setTweetMessage("");
    setTweetImage("");
  };

  return (
    <div className="tweetbox">
      <form>
        <div className="tweetbox-input">
          <Avatar src={pic} />
          <input
            placeholder="What's Happening?"
            type="text"
            value={tweetMessage}
            onChange={(e) => setTweetMessage(e.target.value)}
          />
        </div>
        <input
          placeholder="Enter Image URL"
          type="text"
          className="tweetbox-image"
          value={tweetImage}
          onChange={(e) => setTweetImage(e.target.value)}
        />

        <Button type="submit" className="tweetbox-button" onClick={sendTweet}>
          Tweet
        </Button>
      </form>
    </div>
  );
}

export default TweetBox;
