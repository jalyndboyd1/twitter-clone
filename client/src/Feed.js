import React, { useEffect, useState } from "react";
import "./Feed.css";
import Post from "./Post";
import TweetBox from "./TweetBox";
import db from "./firebase";
import FlipMove from "react-flip-move";

function Feed() {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    // Fetched data from DB
    db.collection("posts").onSnapshot((snapshot) => {
      setPosts(snapshot.docs.map((doc) => doc.data()));
      // returns an array of the data in the selected collection
    });
  }, []);

  console.log(posts);
  return (
    <div className="feed">
      {/* Header */}
      <div className="feed-header">
        <h2>Home</h2>
      </div>
      <TweetBox />
      <FlipMove>
        {posts.map((post) => {
          return (
            <Post
              key={Math.random()}
              displayName={post.displayName}
              username={post.username}
              verified={post.avatar}
              avatar={post.avatar}
              text={post.text}
              image={post.image}
            />
          );
        })}
      </FlipMove>
    </div>
  );
}

export default Feed;
