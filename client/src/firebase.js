import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyByFTVrVTVZevaTLj68vqLxO0ryRXpn6IA",
  authDomain: "twitter-clone-ea36e.firebaseapp.com",
  projectId: "twitter-clone-ea36e",
  storageBucket: "twitter-clone-ea36e.appspot.com",
  messagingSenderId: "96981187898",
  appId: "1:96981187898:web:8673f412363a8e74f66403",
  measurementId: "G-0SH06YFP3E",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export default db;
